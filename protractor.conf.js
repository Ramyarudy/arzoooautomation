const { browser, element } = require("protractor");

exports.config = {
    framework : 'mocha',
    capabilities : {'browserName':'chrome'},
    specs : ["C:\\Users\\DELL\\Documents\\Arzoootests\\test\\appTest.js"],
    directConnect : true,
    mochaOpts : {
        timeout : 30000,
        slow : 5000
    }
};
// onPrepare : () => {
//     browser.get("https://admintest.arzooo.com/login");
//     browser.manage().window().maximize();
//     browser.manage().timeouts.implicitlyWait(10000);
//     element(by.xpath("//span[text()='Log in with Google']")).click();
//     console.log("hello");
//     browser.sleep(20000);
// }